# Nomad/Consul/Docker infrastructure

The code in this repository builds up an infrastructure on which you can deploy containerised services and applications.

The containers run on Docker, the service mesh network is controlled by Consul, and the orchestration is performed by Nomad.

The virtual machine image for each node is built by Packer, and it includes only the necessary binaries (docker, consul, nomad).

## First time setup

For MacOS:

	$ brew install jq
	$ brew install packer
	$ brew install nomad
	$ brew cask install virtualbox
	$ brew cask install vagrant

## Making the base images

If you just want to spin up the cluster, then you will not need to make the base image.  But if you want to fiddle with the image:

	$ cd images
	$ cat readme.md

Follow the instructions.

## Using base images locally

To download the new version of the box, do

	$ vagrant box update

To start a local cluster of machines:

	$ vagrant up

The first time, you might get an error because some plugins have just been downloaded.  Just run it again.

Get the IPs of the hosts:

	$ vagrant hosts list

To login:

	$ vagrant ssh manager-1
	$ vagrant ssh manager-2
	$ vagrant ssh manager-3

To connect to the services:

	- [Nomad](http://localhost:4646)
	- [Consul](http://localhost:8500)

## Examples

	$ cd tests
	$ cat readme.md

Follow the instructions.

## Cleanup

To take everything down and clean up:

	$ vagrant destroy -f

## Todo

- Deploy a load balancer with Nomad, or with Consul Connect, and make the ip-and-counter available to the outside.  Reading https://www.burgundywall.com/post/nomad-sidecars
- Deploy OpenFaas.
- Worker nodes with Nomad & Consul clients.
- ACLs for Consul and Nomad.
- Add Vault cluster.
- Nomad clients just be in docker group, and be run as root?
- Make sure that the right version of VirtualBox GuestAdditions are installed in the images.  Right now it seems that 5.0.18 is installed, but it should be 6.0.10.  Use /opt/VBoxGuestAdditions-6.0.10/ ?  Need vboxsf?  See https://github.com/dotless-de/vagrant-vbguest/issues/300
- Make ntpd work, also after reboots.
- Is there a "deploy-on-all-nodes" configuration in Nomad, like "global" in Docker Swarm or DaemonSet in Kubernetes.
- `docker network create -d overlay --attachable nomad`
