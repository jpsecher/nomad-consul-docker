# Provisioning with Ansible

The final provisioning of the hosts is done by Vagrant via Ansible.  That way, I can build up a set of Ansible roles and playbooks that can serve as a starting point for cloud-based infrastructure.

