job "counter-fabio" {
  datacenters = ["vagrant"]
  update {
    max_parallel = 1
  }
  group "tools" {
    count = 3
    constraint {
      distinct_hosts = true
    }
    network {
      mode = "bridge"
      port "http" {
        to = "8080"
      }
    }
    service {
      name = "ipandcount"
      port = "http"
      connect {
        sidecar_service {}
      }
      # tags = ["urlprefix-/counter"]
      # check {
      #   type     = "http"
      #   path     = "/"
      #   interval = "10s"
      #   timeout  = "2s"
      # }
    }
    task "counter" {
      driver = "docker"
      config {
        image = "jpsecher/ip-and-counter"
        args = ["${node.unique.name}"]
      }
    }
  }
}
