# Nomad tests

## Sanity check with trivial service

Run a trivial service like this:

    $ nomad run trivial.nomad
    $ nomad status trivial

To contact the trivial service, you have to log into one of the VM hosts and find the dynamic address/port of the container `ipcount`:

    $ vagrant ssh manager-1
    $ docker ps | grep ipcount
    ... 10.0.2.15:31017->8080/tcp ...

With that you can query the trivial service:

    $ curl 10.0.2.15:31017
    server: manager-1
    Container IP: 172.17.0.2
    Visits: 2
    Host: 10.0.2.15:31017

Stop like this:

    $ nomad stop trivial

## Service Mesh

Run the Dashboard/Counter example:

    $ nomad run countdash.nomad

Find out where the dashboard runs:

    $ nomad status countdash
    ID        Node ID   Task Group  Version  Desired  Status   Created     Modified
    ba4d183b  4debb5c3  dashboard   0        run      running  20m21s ago  19m46s ago
    ...

Match the node id with the node name:

    $ nomad node status
    ID        DC       Name       Class   Drain  Eligibility  Status
    4debb5c3  vagrant  manager-3  <none>  false  eligible     ready
    ...

Find the SSH port of the node:

    $ vagrant ssh-config
    Host manager-3
      HostName 127.0.0.1
      User vagrant
      Port 2205
      ...

Make a tunnel to the service af port 9002:

    $ ssh -N -L 9002:localhost:9002 -i ../.vagrant/machines/manager-3/virtualbox/private_key vagrant@127.0.0.1 -p 2205

Now you can connect your browser to `http://localhost:9002`

## Load balancer

    $ nomad run loadbalancer.nomad

Now the load balancer should respond with 404 (because no service is running):

    $ curl http://$(vagrant hosts list | head -1 | awk '{print $1}'):8080

And the dashboard shoul be working:

    $ curl -L http://$(vagrant hosts list | head -1 | awk '{print $1}'):8081

Now let's run a counter on each host:

    $ nomad run counter-lb.nomad


## Notes

    - To get address of service: eg. `dig @127.0.0.1 -p 8600  traefik-ui.service.vagrant.consul A +short` inside a host.
