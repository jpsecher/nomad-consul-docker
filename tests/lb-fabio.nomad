job "lb-fabio" {
  datacenters = ["vagrant"]
  update {
    max_parallel = 1
  }
  group "infrastructure" {
    count = 3
    constraint {
      distinct_hosts = true
    }
    network {
      mode = "bridge"
      port "http" {
        to = 9999
        static = 9999
      }
      port "ui" {
        to = 9998
        static = 9998
      }
    }
    service {
      name = "reverse-proxy"
      port = "http"
      connect {
        sidecar_service {}
      }
    }
    task "fabio" {
      driver = "docker"
      config {
        image = "fabiolb/fabio"
        #network_mode = "host"
      }
    }
  }
}
