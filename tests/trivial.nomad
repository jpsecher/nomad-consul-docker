job "trivial" {
  datacenters = ["vagrant"]
  update {
    max_parallel = 1
  }
  group "tools" {
    count = 3
    constraint {
      distinct_hosts = true
    }
    task "ipcount" {
      driver = "docker"
      config {
        image = "jpsecher/ip-and-counter"
        args = [
          "${node.unique.name}"
        ]
        port_map {
          web = 8080
        }
      }
      resources {
        network {
          port "web" {}
        }
      }
      service {
        name = "ipcount"
        tags = ["global", "tools"]
        port = "web"
        check {
          name = "alive"
          type = "tcp"
          interval = "10s"
          timeout = "2s"
        }
      }
    }
  }
}
