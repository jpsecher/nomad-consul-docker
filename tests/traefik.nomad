job "traefik" {
  datacenters = ["vagrant"]
  update {
    max_parallel = 1
  }
  group "infrastructure" {
    count = 3
    constraint {
      distinct_hosts = true
    }
    network {
      mode = "bridge"
      port "web" {
        static = 80
        to = 80
      }
      port "ui" {
        static = 8080
        to = 8080
      }
    }
    service {
      name = "traefik"
      port = "web"
    }
    service {
      name = "traefik-ui"
      port = "ui"
    }
    service {
      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "counter"
              local_bind_port = 9876
            }
          }
        }
      }
    }
    task "traefik" {
      driver = "docker"
      config {
        image = "traefik:2.1"
        args = [
          "--api.insecure=true",
          "--entrypoints.web.address=:80",
          "--providers.file.directory=/etc/traefik.d",
        ]
        volumes = [
          "local/counter.yml:/etc/traefik.d/counter.yml",
        ]
      }
      template {
        data = <<-EOF
          http:
            routers:
              counter:
                rule: Host(`counter`)
                service: counter
            services:
              counter:
                loadBalancer:
                  servers:
                    - url: http://{{ env "NOMAD_UPSTREAM_ADDR_counter" }}
          EOF
        destination = "local/counter.yml"
      }
    }
  }
}
