job "counter" {
  datacenters = ["vagrant"]
  group "tools" {
    count = 3
    constraint {
      distinct_hosts = true
    }
    network {
      mode = "bridge"
      port "http" {
        to = "8080"
      }
    }
    service {
      name = "counter"
      port = "http"
      connect {
        sidecar_service {}
      }
      check {
        type     = "http"
        path     = "/health"
        interval = "10s"
        timeout  = "1s"
      }
    }
    task "counter" {
      driver = "docker"
      config {
        image = "jpsecher/ip-and-counter"
        args = ["${node.unique.name}"]
        #network_mode = "nomad"
      }
    }
  }
}
