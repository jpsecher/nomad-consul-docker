#!/bin/bash -eux

## NOTE: this will install an old version of Virtualbox Additions.
echo "--> Installing Virtualbox kernel module"
sudo apt-get install -y virtualbox-guest-dkms
