#!/bin/bash -eux

echo "--> Install Virtualbox Guest Additions"
VBOX_ISO=VBoxGuestAdditions.iso
sudo mount -o loop $VBOX_ISO /mnt
yes | sudo /mnt/VBoxLinuxAdditions.run || true

## Cleanup
USER=vagrant
sudo umount /mnt
sudo rm /home/${USER}/VBoxGuestAdditions*.iso
sudo rm /home/${USER}/.vbox_version

