#!/bin/ash -eux

sed -i -E 's,#?(UseDNS\s+).+,\1no,' /etc/ssh/sshd_config
