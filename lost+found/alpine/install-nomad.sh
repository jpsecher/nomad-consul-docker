#!/bin/ash -eux

apk add procps
apk add libc6-compat

cat << EOF > nomad-zip.sha256
bc9ce5594ed18e97909b0a6581ff8452bb251dd54f901c9e1f0bf5bb18cf0d4b  -
EOF
curl --location --silent --fail https://releases.hashicorp.com/nomad/0.10.0-connect1/nomad_0.10.0-connect1_linux_amd64.zip | tee nomad.zip | sha256sum -c nomad-zip.sha256
unzip -o nomad.zip
mv -f nomad /usr/local/bin/nomad
chmod +x /usr/local/bin/nomad
rm -f nomad*
