#!/bin/ash -eux

cat << EOF > consul-zip.sha256
bc9ce5594ed18e97909b0a6581ff8452bb251dd54f901c9e1f0bf5bb18cf0d4b  -
EOF
curl --location --silent --fail https://releases.hashicorp.com/consul/1.6.0-beta1/consul_1.6.0-beta1_linux_amd64.zip | tee consul.zip | sha256sum -c consul-zip.sha256
unzip -o consul.zip
mv -f consul /usr/local/bin/consul
chmod +x /usr/local/bin/consul
rm -f consul*
