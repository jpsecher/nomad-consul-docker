#!/bin/ash -eux

echo http://mirrors.dotsrc.org/alpine/v3.10/community >>/etc/apk/repositories
apk add -U virtualbox-guest-additions virtualbox-guest-modules-vanilla
rc-update add virtualbox-guest-additions
echo vboxsf >>/etc/modules
modinfo vboxguest
