# Images

There is currenly only one base image.  The image is built on top of Ubuntu 16.04.

## Making the base images

	$ make

## Publishing the image to Vagrant Cloud

    $ vagrant cloud auth login
    $ make publish

The published name must match that in `Vagrantfile.template`.

See the [Vagrant Cloud UI](https://app.vagrantup.com/) for the result.

## Notes

To locally test the image without uploading and then downloading it:

	$ make add-box

## Todo

- Perform package-manager/stop-apt-daily-service ansible playbook here when building image.
- Run `apt -y autoremove` early and reboot, to make sure that only the newest kernel is installed.  And `service ntp restart`.
- "Copy iso file /Applications/VirtualBox.app/Contents/MacOS/VBoxGuestAdditions.iso into the box /tmp/VBoxGuestAdditions.iso, Mounting Virtualbox Guest Additions ISO to: /mnt, mount: /dev/loop0 is write-protected, mounting read-only, Installing Virtualbox Guest Additions 6.0.10 - guest version is 5.0.18"
