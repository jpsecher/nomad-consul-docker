#!/bin/bash -eux

echo "--> Installing Nomad CNI plugins"

cat << EOF > cni-plugins-tgz.sha256
29a092bef9cb6f26c8d5340f3d56567b62c7ebdb1321245d94b1842c80ba20ba  -
EOF
curl --location --silent --fail https://github.com/containernetworking/plugins/releases/download/v0.8.3/cni-plugins-linux-amd64-v0.8.3.tgz | tee cni-plugins.tgz | sha256sum -c cni-plugins-tgz.sha256
sudo mkdir -p /opt/cni/bin
sudo tar -C /opt/cni/bin -xzf cni-plugins.tgz
rm -f cni-plugins*
