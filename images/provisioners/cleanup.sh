#!/bin/bash -eux

echo "--> APT autoremove"
sudo apt -y autoremove

# echo "--> Cleaning up udev rules"
# sudo rm -rf /dev/.udev/
# sudo rm /lib/udev/rules.d/75-persistent-net-generator.rules

echo "--> Cleaning up leftover dhcp leases"
if [ -d "/var/lib/dhcp" ]; then
    sudo rm /var/lib/dhcp/*
fi

echo "--> Add delay to prevent 'vagrant reload' from failing"
sudo sh -c "echo 'pre-up sleep 2' >> /etc/network/interfaces"

echo "--> Cleaning up tmp & cache"
sudo rm -rf /tmp/*
sudo apt-get -y autoremove --purge
sudo apt-get -y clean
sudo apt-get -y autoclean

echo "--> Show installed packages"
dpkg --get-selections | grep -v deinstall

echo "--> Clear logs"
unset HISTFILE
sudo rm -f /root/.bash_history
sudo rm -f ~/.bash_history

sudo sh -c "find /var/log -type f | while read f; do echo -ne '' > \$f; done"

echo "--> Whiteout tmp partition"
count=$(df --sync -kP / | tail -n1  | awk -F ' ' '{print $4}')
let count--
sudo dd if=/dev/zero of=/tmp/whitespace bs=1024 count=$count
sudo rm /tmp/whitespace

echo "--> Whiteout boot partition"
count=$(df --sync -kP /boot | tail -n1 | awk -F ' ' '{print $4}')
let count--
sudo dd if=/dev/zero of=/boot/whitespace bs=1024 count=$count
sudo rm /boot/whitespace

echo "--> Whiteout free space on root partition"
# Zero out the free space to save space in the final image
sudo dd if=/dev/zero of=/EMPTY bs=1M || true
sudo rm -f /EMPTY

# Make sure we wait until all the data is written to disk, otherwise
# Packer might quite too early before the large files are deleted
sudo sync
