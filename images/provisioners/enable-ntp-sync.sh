#!/bin/bash -e

echo "--> Enabling time sync via Network Time Protocol"
sudo apt-get -y install ntp

## Make sure ntpd does not exit if time has drifted a lot.
sudo sh -c 'echo "tinker panic 0" >> /etc/ntp.conf'

sudo service ntp start
# sudo timedatectl set-ntp on
