#!/bin/bash -eux

VERSION=1.3.0

echo "--> Installing Vault $VERSION"

cat << EOF > vault-zip.sha256
d89b8a317831b06f2a32c56cb86071d058b09d9317b416bb509ce3d01e912eb3  -
EOF
curl --location --silent --fail https://releases.hashicorp.com/vault/${VERSION}/vault_${VERSION}_linux_amd64.zip | tee vault.zip | sha256sum -c vault-zip.sha256
unzip -o vault.zip
chmod +x vault
sudo mv -f vault /usr/local/bin/vault
sudo chown root:root /usr/local/bin/vault
rm -f vault*
