#!/bin/bash -eux

echo "--> Removing unnecessary packages"

sudo apt-get -y purge \
	console-setup \
	crda \
	eject \
	gcc \
	git-man \
	install-info \
	installation-report \
	iw \
	keyboard-configuration \
	krb5-locales \
	language-pack-en \
	laptop-detect \
	libmpdec2 \
	libpci3 \
	libusb-1.0-0 \
	manpages \
	pciutils \
	pigz \
	rename \
	rsync \
	tasksel \
	ubuntu-advantage-tools \
	usbutils \
	wireless-regdb

sudo apt -y autoremove > /dev/null
