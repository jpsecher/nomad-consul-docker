#!/bin/bash -eux

echo
echo "--> Adding vagrant user as TTY-less SUDOer"
USER=vagrant
SUDOERS=/etc/sudoers.d/$USER
echo "$USER ALL=(ALL) NOPASSWD:ALL" > "$SUDOERS"
echo "Defaults:$USER !requiretty" >> "$SUDOERS"
chmod 440 "$SUDOERS"
