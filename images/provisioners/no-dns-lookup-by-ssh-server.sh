#!/bin/bash -eux

echo "--> SSH daemon must not use DNS lookups because they are slow"
sudo sh -c "echo 'UseDNS no' >> /etc/ssh/sshd_config"
