#!/bin/bash -eux

VERSION=0.10.2

echo "--> Installing Nomad $VERSION"

cat << EOF > nomad-zip.sha256
cab179b0245e07ed39f4bcaa543e88d32fadf8660c2230733ce836873fe2714d  -
EOF
curl --location --silent --fail https://releases.hashicorp.com/nomad/${VERSION}/nomad_${VERSION}_linux_amd64.zip | tee nomad.zip | sha256sum -c nomad-zip.sha256
unzip -o nomad.zip
chmod +x nomad
sudo mv -f nomad /usr/local/bin/nomad
sudo chown root:root /usr/local/bin/nomad
rm -f nomad*
