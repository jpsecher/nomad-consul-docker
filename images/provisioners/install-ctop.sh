#!/bin/bash -eu

echo "--> Installing ctop"

cat << EOF > ctop.sha256
e1af73e06f03caf0c59ac488c1cda97348871f6bb47772c31bbd314ddc494383  -
EOF
curl --location --silent --fail https://github.com/bcicen/ctop/releases/download/v0.7.2/ctop-0.7.2-linux-amd64 | tee ctop | sha256sum -c ctop.sha256
chmod +x ctop
sudo mv -f ctop /usr/local/bin/ctop
sudo chown root:root /usr/local/bin/ctop
rm -f ctop*
