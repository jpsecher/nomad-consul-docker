#!/bin/bash -eux

echo "--> Clearing APT cache for Official Ubuntu ISO images"
sudo rm /var/lib/apt/lists/*.archive.ubuntu.com_ubuntu_dists_*
