#!/bin/bash -e

echo "--> Installing Docker"
sudo apt-get -y install docker.io
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker `id -un`
