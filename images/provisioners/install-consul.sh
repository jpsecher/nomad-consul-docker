#!/bin/bash -eux

VERSION=1.7.0-beta2

echo "--> Installing Consul $VERSION"

cat << EOF > consul-zip.sha256
adaa0d7fab4a148c09fa7304bcce984f3ac516e90cd3762f393229bd33b64d7b  -
EOF
curl --location --silent --fail https://releases.hashicorp.com/consul/${VERSION}/consul_${VERSION}_linux_amd64.zip | tee consul.zip | sha256sum -c consul-zip.sha256
unzip -o consul.zip
chmod +x consul
sudo mv -f consul /usr/local/bin/consul
sudo chown root:root /usr/local/bin/consul
rm -f consul*

## Already set by default.
#sudo sh -c 'echo 1 > /proc/sys/net/bridge/bridge-nf-call-arptables'
#sudo sh -c 'echo 1 > /proc/sys/net/bridge/bridge-nf-call-ip6tables'
#sudo sh -c 'echo 1 > /proc/sys/net/bridge/bridge-nf-call-iptables'
